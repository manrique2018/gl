#include <windows.h>
#include <GL/glut.h>
#include <math.h>

GLfloat LIGHT_POS = 50.0f; //The length of each side of the cube
GLfloat BOX_HEIGHT = 7.0f; //The height of the box off of the ground
GLfloat FLOOR_SIZE = 25.0f; //The length of each side of the floor
GLfloat Zoom = 1;
GLfloat Luz = 0.5;
GLfloat Transparencia = 150; // reflejo del piso
GLint spinX = 0, spinY = 0;
GLint Rot = 0;
GLfloat Rotacion = 0;
/*******************************************************/
GLfloat Inclinacion = 30;
GLfloat zoom=0.3;
GLfloat x=2;
GLfloat y=76;
GLfloat z=76;
GLfloat TamaPiso=3;
GLfloat x2 = 0;       //Posici�n en x
GLfloat y2 = -7.6;   //Posici�n en y ---- Debe ajustar a su escena
GLfloat z2 = 0;       //Posici�n en z
GLfloat Angulo = 0;
GLfloat r = 0;
GLfloat reescalar;

/*******************************************************/
void DibujaEsfera (void) {
    reescalar=0.05;
    glScalef(reescalar, reescalar,reescalar);
glTranslatef(0,-60,0);
glRotatef(Rotacion, 0, -1, 0);
glRotatef(Inclinacion,1,0,0);
glTranslatef(x2,y2,0);

    GLUquadricObj * Objeto;
    GLUquadricObj * Objeto1;
    GLUquadricObj * Objeto2;
    GLUquadricObj * Objeto3;
    GLUquadricObj * Objeto4;
    GLUquadricObj * Objeto5;
    GLUquadricObj * Objeto6;
    GLUquadricObj * Objeto7;
    GLUquadricObj * Objeto8;
    GLUquadricObj * Objeto9;
    GLUquadricObj* quadratic;


  glPushMatrix();
    Objeto = gluNewQuadric();
    gluQuadricDrawStyle(Objeto,GLU_FILL);
    glTranslatef(0,0,0);
    glColor4ub(255, 255, 0,0);
    glRotatef(90,-1,0,0);
    gluCylinder(Objeto,8,8,5,20,1);
  glPopMatrix();

   glPushMatrix();
    quadratic = gluNewQuadric();
    glTranslatef(0,5,0);
    glRotatef(90,-1,0,0);
    glColor4ub(255, 0, 0,0);
    gluDisk(quadratic,0,9,10,32);
  glPopMatrix();

//tapa
 glPushMatrix();
    quadratic = gluNewQuadric();
    glTranslatef(0,0,0);
    glRotatef(90,-1,0,0);
    glColor4ub(255, 0, 0,0);
    gluDisk(quadratic,0,25,3,32);
  glPopMatrix();


 glPushMatrix();
    Objeto1 = gluNewQuadric();
    gluQuadricDrawStyle(Objeto1,GLU_FILL);
     glColor4ub(255, 255, 0,0);
    glTranslatef(0,-5,0);
    glRotatef(90,-1,0,0);
    gluCylinder(Objeto1,32,25,5,20,1);
 glPopMatrix();

  glPushMatrix();
    Objeto2 = gluNewQuadric();
    gluQuadricDrawStyle(Objeto2,GLU_FILL);
    glTranslatef(0,-10,0);
     glColor4ub(255, 0, 0,0);
    glRotatef(90,-1,0,0);
    gluCylinder(Objeto2,34,32,5,20,1);
 glPopMatrix();

   glPushMatrix();
    Objeto3 = gluNewQuadric();
    gluQuadricDrawStyle(Objeto3,GLU_FILL);
    glTranslatef(0,-15,0);
     glColor4ub(255, 255, 0,0);
    glRotatef(90,-1,0,0);
    gluCylinder(Objeto3,36,34,5,20,1);
 glPopMatrix();

   glPushMatrix();
    Objeto4 = gluNewQuadric();
    gluQuadricDrawStyle(Objeto4,GLU_FILL);
    glTranslatef(0,-20,0);
     glColor4ub(255, 0, 0,0);
    glRotatef(90,-1,0,0);
    gluCylinder(Objeto4,36,36,5,20,1);
  glPopMatrix();

   glPushMatrix();
    Objeto5 = gluNewQuadric();
    gluQuadricDrawStyle(Objeto5,GLU_FILL);
    glTranslatef(0,-25,0);
     glColor4ub(255, 255, 0,0);
    glRotatef(90,-1,0,0);
    gluCylinder(Objeto5,34,36,5,20,1);
  glPopMatrix();

glPushMatrix();
    Objeto6 = gluNewQuadric();
    gluQuadricDrawStyle(Objeto6,GLU_FILL);
    glTranslatef(0,-30,0);
     glColor4ub(255, 0, 0,0);
    glRotatef(90,-1,0,0);
    gluCylinder(Objeto6,32,34,5,20,1);
  glPopMatrix();

  glPushMatrix();
    Objeto7 = gluNewQuadric();
    gluQuadricDrawStyle(Objeto7,GLU_FILL);
    glTranslatef(0,-75,0);
    glColor4ub(255, 255, 0,0);
    glRotatef(90,-1,0,0);
    gluCylinder(Objeto7,0,32,45,20,1);
  glPopMatrix();

glColor4ub(42, 51, 49,0);
glPushMatrix();
    Objeto8 = gluNewQuadric();
    gluQuadricDrawStyle(Objeto8,GLU_FILL);
    glTranslatef(0,-78,0);
    glRotatef(90,-1,0,0);
    gluCylinder(Objeto8,1,1,5,20,1);
  glPopMatrix();

  glPushMatrix();
    Objeto9 = gluNewQuadric();
    gluQuadricDrawStyle(Objeto9,GLU_FILL);
    glTranslatef(0,-80,0);

    glRotatef(90,-1,0,0);
    gluCylinder(Objeto9,0,2,5,20,1);
  glPopMatrix();

}
/*******************************************************/

void drawFloor(){
    GLfloat Lado = 12;
    GLfloat Brick = Lado/8;
    GLint Cuadros = 18;

    glPushMatrix();
    glTranslatef(-Brick, 0, 0);
    glBegin(GL_QUADS);
    for(int i=0;i<Cuadros;i++){
       for(int j=0;j<Cuadros;j++){
            if((i+j)%2==0){
                glColor4ub(25, 25, 112,Transparencia);
            }else{
                glColor4ub(112, 112, 112,Transparencia);
            }
                glTexCoord2f(0, 0); glVertex3f(-Lado+(i*Brick), 0, -Lado+(j*Brick));
                glTexCoord2f(1, 0); glVertex3f(-(Lado-Brick)+(i*Brick), 0, -Lado+(j*Brick));
                glTexCoord2f(1, 1); glVertex3f(-(Lado-Brick)+(i*Brick), 0, -(Lado-Brick)+(j*Brick));
                glTexCoord2f(0, 1); glVertex3f(-Lado+(i*Brick), 0, -(Lado-Brick)+(j*Brick));
        }
    }
    glEnd();
    glPopMatrix();
}

void drawUmbrella (void) {

   GLint Lados = 30;

   GLUquadricObj *qobj; //Define el objeto
   qobj = gluNewQuadric();
   gluQuadricDrawStyle(qobj, GLU_FILL); //Se dibuja un trompo

    GLfloat ambient[]   = { 0.20, 0.05, 0.05, 1.0 };
    GLfloat diffuse[]   = { 0.89, 0.64, 0.14, 1.0 };
    GLfloat specular[]  = { 0.00, 0.00, 0.00, 1.0 };
    GLfloat emission[]  = { 0.00, 0.00, 0.00, 1.0 };
    GLfloat shininess[] = { 128.0 };

    glMaterialfv( GL_FRONT, GL_AMBIENT,   ambient );
    glMaterialfv( GL_FRONT, GL_DIFFUSE,   diffuse );
    glMaterialfv( GL_FRONT, GL_SPECULAR,  specular );
    glMaterialfv( GL_FRONT, GL_EMISSION,  emission );
    glMaterialfv( GL_FRONT, GL_SHININESS, shininess );

   glPushMatrix();
     glTranslatef(0, -3.7 ,0);
     glRotatef(45, 1, 0, 0);
     glColor4ub(255, 0, 0, 0); //Capa roja
     gluCylinder(qobj, 0, 3.9, 1, 11, 1);
     glColor4ub(100, 100, 100, 0); //Palo gris
     gluCylinder(qobj, 0.05, 0.05, 4.7, 10, 1);
   glPopMatrix();

   gluDeleteQuadric(qobj);

}


void drawScene() {
  glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT | GL_STENCIL_BUFFER_BIT);

  GLfloat mat_specular[] = { 1.0, 1.0, 1.0, 1.0 };
  GLfloat diffuseMaterial[] = { 1.0, 1.0, 1.0, 0.0 };
  GLfloat lmodel_ambient[] = { Luz, Luz, Luz, Luz };

  GLfloat lightColor[] = {1.0f, 1.0f, 1.0f, 1.0f};

  GLfloat lightPos[] = {LIGHT_POS/2, LIGHT_POS, LIGHT_POS/2, 0};

  glMatrixMode(GL_MODELVIEW);
  glLoadIdentity();

  glTranslatef(0, 0, -40);
  glRotatef(30, 1, 0, 0);

  glScalef(Zoom, Zoom, Zoom);

  glRotatef(spinX, 1, 0, 0);
  glRotatef(spinY, 0, 1, 0);

  if (Rot == 1)
      glRotatef(Rotacion, 0, 1, 0);

  glLightfv(GL_LIGHT0, GL_DIFFUSE, lightColor);
  glLightfv(GL_LIGHT0, GL_POSITION, lightPos);

  glMaterialfv(GL_FRONT, GL_DIFFUSE, diffuseMaterial);
  glMaterialfv(GL_FRONT, GL_SPECULAR, mat_specular);
  glMaterialf(GL_FRONT, GL_SHININESS, 64.0f);
  glLightModelfv(GL_LIGHT_MODEL_AMBIENT, lmodel_ambient);

  glPushMatrix();
    glColor4ub(255, 255, 0, 0);
    glTranslatef(0, LIGHT_POS, 0);
    glutSolidSphere(0.3, 50, 50);
  glPopMatrix();

  glPushMatrix();
    glTranslatef(0, BOX_HEIGHT, 0);
    //Aqu� debe llamar a los objetos
    //drawUmbrella();
    DibujaEsfera();
  glPopMatrix();

  glEnable(GL_STENCIL_TEST); //Enable using the stencil buffer
  glColorMask(0, 0, 0, 0); //Disable drawing colors to the screen
  glDisable(GL_DEPTH_TEST); //Disable depth testing
  glStencilFunc(GL_ALWAYS, 1, 1); //Make the stencil test always pass

  //Make pixels in the stencil buffer be set to 1 when the stencil test passes
  glStencilOp(GL_KEEP, GL_KEEP, GL_REPLACE);

  //Aqu� se dibuja el piso
  drawFloor();

  glColorMask(1, 1, 1, 1); //Enable drawing colors to the screen
  glEnable(GL_DEPTH_TEST); //Enable depth testing

  //Make the stencil test pass only when the pixel is 1 in the stencil buffer
  glStencilFunc(GL_EQUAL, 1, 1);
  glStencilOp(GL_KEEP, GL_KEEP, GL_KEEP); //Make the stencil buffer not change

  //Draw the cube, reflected vertically, at all pixels where the stencil
  //buffer is 1
  glPushMatrix();
    glScalef(1, -1, 1);
    glTranslatef(0, BOX_HEIGHT, 0);
    //Aqu� se dibujan los objetos que se van a reflejar

   // drawUmbrella();
    DibujaEsfera();
  glPopMatrix();

  glDisable(GL_STENCIL_TEST); //Disable using the stencil buffer
  //Blend the floor onto the screen
  glEnable(GL_BLEND);

  //Aqu� se dibuja el piso que refleja
  drawFloor();
  glDisable(GL_BLEND);

  glutSwapBuffers();
}

void handleKeypress(unsigned char key, int x, int y) {
  switch (key) {
    case 27: //Escape key
         exit(0);
    break;

    case 'F': //Posici�n de la luz
        ++LIGHT_POS;
    break;

    case 'f': //Posici�n de la luz
        --LIGHT_POS;
    break;

    case 'r': //La esfera gira
         if (Rot == 1)
             Rot = 0;
         else
             Rot = 1;
    break;

    case 'l': //Luz
         Luz -= 0.005;
         if (Luz < 0)
             Luz = 1;
    break;

    case 'h':
        if (Zoom > 0)
            Zoom -= 0.01;
    break;

    case 'H':
        if (Zoom < 10)
            Zoom += 0.01;
    break;

    case 't': //Sube
         if (++Transparencia > 255)
             Transparencia = 0;
    break;
  }
}

void init() {
  glClearColor(0, 0, 0, 0); //Fondo negro en toda la escena
  glEnable(GL_DEPTH_TEST);
  glEnable(GL_LIGHTING);
  glEnable(GL_LIGHT0);
  glEnable(GL_NORMALIZE);
  glEnable(GL_COLOR_MATERIAL);
  glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);

}

void handleResize(int w, int h) {
  glViewport(0, 0, w, h);
  glMatrixMode(GL_PROJECTION);
  glLoadIdentity();
  gluPerspective(45.0, (float)w / (float)h, 1.0, 200.0);
}

void spinDisplay(void) {
   Rotacion += 1;
      if (Rotacion >= 360) {
          Rotacion = 0;
   }

   if(Inclinacion > 0){
    Inclinacion-=0.001;
   }
   else {
   Inclinacion=30;}

   Angulo += 0.002;
   if (Angulo > 360) {
       Angulo -= 360;
   }

   //Espiral de Bernoulli (logaritmica)
   r = 100 - pow (2.7182, 0.1 * Angulo);     //Debe ajustar a su escena
   if (0 < r) {
       x2 = r * cos (Angulo);
       z2 = r * sin (Angulo);
       //printf("r = %6.4f ang = %5.2f x=%5.2f z=%5.2f\n", r, Angulo, x, z);
   }

   glutPostRedisplay(); //Vuelve a dibujar
}

void mouse(int button, int state, int x, int y) {
   switch (button) {
      case GLUT_LEFT_BUTTON:
         if (state == GLUT_DOWN) {
            glutIdleFunc(spinDisplay);
         }
         break;

      case GLUT_RIGHT_BUTTON:
      case GLUT_MIDDLE_BUTTON:
         if (state == GLUT_DOWN)
            glutIdleFunc(NULL);
         break;

      default:
         break;
   }
}
void mouseMotion(int x, int y) {
     spinX = y;
     spinY = x;
     //printf("X %5.2f Y %5.2f\n", spinX, spinY);
}

int main(int argc, char** argv) {
  glutInit(&argc, argv);
  glutInitDisplayMode(GLUT_DOUBLE | GLUT_RGB | GLUT_DEPTH | GLUT_STENCIL);
  glutInitWindowSize(600, 600);

  glutCreateWindow("Sombrilla con reflejo");
  init();

  glutDisplayFunc(drawScene);
  glutKeyboardFunc(handleKeypress);
  glutReshapeFunc(handleResize);
  glutMouseFunc(mouse); //Activa los controles del mouse
  glutMotionFunc(mouseMotion);

  glutMainLoop();

  return 0;
}
