  //Plano para proyectar la sombra
  GLfloat shadow_plane[] = { 0.0, 1.0, 0.0, 0.0 };

  glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT | GL_STENCIL_BUFFER_BIT);

  glMatrixMode(GL_MODELVIEW);
  glLoadIdentity();

  glTranslatef(0, 0, -40);
  glRotatef(30, 1, 0, 0);

  glScalef(Zoom, Zoom, Zoom);

  glRotatef(spinX, 1, 0, 0);
  glRotatef(spinY, 0, 1, 0);

  glLightfv( GL_LIGHT0, GL_POSITION, light_position );
  glPushMatrix();
    glTranslatef( light_position[0], light_position[1], light_position[2] );
    drawLight();
  glPopMatrix();

    if ( shadowOn && stencilOn )
    {
       glEnable( GL_STENCIL_TEST );
       glStencilFunc( GL_ALWAYS, 0x1, 0xffffffff );
       glStencilOp( GL_REPLACE, GL_REPLACE, GL_REPLACE );
    }

    drawFloor();

    // Draw shadow of object.  To do this we will only draw
    // where the stencil buffer values are equal to 1.  Furthermore,
    // to avoid "dark shadows" where more than one polygon projects to
    // the same place, we only draw each pixel of the shadow once; this
    // is done by setting the stencil value to 0 when we draw.

    if ( shadowOn )
    {
       glDisable( GL_LIGHTING );
       glDisable( GL_DEPTH_TEST );

        if ( stencilOn )
        {
            glEnable( GL_STENCIL_TEST );
            glStencilFunc( GL_EQUAL, 0x1, 0xffffffff );
            glStencilOp( GL_KEEP, GL_KEEP, GL_ZERO );
        }

        // Set up for blending and define the "shadow color".  We use black,
        // but set the alpha value so that this will be blended with the
        // color already in the color buffer.

        glEnable( GL_BLEND );
        glBlendFunc( GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA );
        glColor4f( 0.0, 0.0, 0.0, shadowAlpha );

        // Draw the actual shadow by modifying the MODELVIEW matrix and
        // then drawing the object that casts the shadow.

        glPushMatrix();
          shadowTransform( shadow_plane, light_position );
          drawCube();
          drawTeaPot();
        glPopMatrix();

        glDisable( GL_BLEND );
        glDisable( GL_STENCIL_TEST );
        glEnable( GL_DEPTH_TEST );
        glEnable( GL_LIGHTING );
    }

    // Draw the object.  With the exception of the call to
    // shadowTransform() the transformations here should match those
    // given above when the shadow was drawn.


    glColor4ub(255, 0, 0, 0);
    drawTeaPot();
    glColor4ub(255, 255, 0, 0);
    drawCube();

    // done drawing
    glutSwapBuffers();
}
