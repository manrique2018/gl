#include <windows.h>
#include <GL/glut.h>
#include <math.h>
#include "TGATextura.h" //Este include administra los archivos TGA
#include <stdio.h>
#include <stdlib.h>

//GLfloat LIGHT_POS = 50.0f; //The length of each side of the cube
//GLfloat BOX_HEIGHT = 7.0f; //The height of the box off of the ground
GLfloat FLOOR_SIZE = 25.0f; //The length of each side of the floor

//Para la luz
GLfloat LIGHT_POS = 20.0f;
GLfloat BOX_HEIGHT = 7.0f;

GLint Lados = 30;

GLint isRightKeyPressed = false;
GLint isLeftKeyPressed  = false;
GLint isUpKeyPressed    = false;
GLint isDownKeyPressed  = false;

GLint ArrowColor;

GLfloat Zoom = 1;
GLint Cubo = 0;

GLfloat Luz = 0.5;
GLfloat Transparencia = 200;

GLfloat aZh = 0;

GLfloat _angle = 0;

GLfloat spinX=0.0, spinY=0.0;

//Direcci�n
GLint dx = 0; GLint dy = -1; GLint dz = 0;

//Posici�n
GLfloat rX = 0; GLfloat rY = 7.5; GLfloat rZ = 0;

//del helicoptero
GLdouble xh = 3, yh = 1.8, zh = -8;  // altura 1.85

GLdouble spinH = 1.0;
bool activarHelicoptero = false;

GLfloat laserControl=0;
GLfloat vellaser=0;
GLint misilControl=0;
GLint heliceControl=0;
unsigned char miraControl;
unsigned char disparoControl = 20;

GLint PlanVuelo = 0; //activa o desactiva el plan de vuelo
GLint k = 0;
char Direccion[50]; //indicacion del plan de vuelo
GLfloat Desplazamiento[50]; //plan de vuelo
GLfloat IncVelocidad = 0.05; //velocidad de ejecucion de los comandos que lee
GLfloat mDesplazamiento = 0;
GLint MaxPunt; //Cant de comandos leidos

GLfloat AnguloX=0.0;
GLfloat AnguloY=0.0;
GLfloat AnguloZ=0.0;

GLfloat EjeX=-40;
GLfloat EjeY=-95;
GLfloat EjeZ=-95;

GLint Rot = 0;
GLfloat Rotacion = 0;

//esto es del trompo

GLdouble r = 0.0;

GLdouble x = 2;
GLdouble y = -6.8;
GLdouble z = 8;

GLdouble inclinacion = 0.0;
GLdouble Angulo = 0.0;
////
GLdouble uu = 25;
GLdouble spinT = 0.0;
bool activarTrompo = false;

//esto es de la copa
GLfloat lines = 12;

//esto es de la bola
GLfloat rotate;
GLfloat cerca = 0;
GLint Meridianos = 0;

GLfloat xBall=0;
GLfloat yBall=0;
GLfloat zBall=0;
GLfloat BallControl=0;
GLfloat BallRot=0;
GLfloat BallVelocidad=0.05;
GLfloat BallSube=0;
GLfloat BallLimite=2;

GLfloat bolaControl = -25;
bool activarBola = false;
GLUquadricObj *Cilindro;
GLfloat orbitDegrees = 0;





//de sombras
bool TeaPot = true;
GLfloat light_position[] = { 0.0, 10.0, 0.0, 0.0 };
float alpha  = 0.0;        // object rotation angle (degrees)

bool stencilOn = true;        // determines if stencil buffer is used
bool shadowOn  = true;        // determines if shadow is to be drawn
float shadowAlpha = 0.7;    // darkness of shadow

void shadowTransform( float n[], float light[] ){
    float m[16];
    float k;
    int i, j;

    k = n[0] * light[0] + n[1] * light[1] + n[2] * light[2] + n[3] * light[3];

    for ( i = 0; i < 4; i++ )
    {
        for ( j = 0; j < 4; j++ )
        {
            m[4*i+j] = -n[i] * light[j];
        }
        m[5*i] += k;    /* Add k to diagonal entries */
    }

    glMultMatrixf( m );
}

void drawFloor(){
    GLfloat Lado = 12;
    GLfloat Brick = Lado/8;

    GLfloat ambient[]   = { 0.1, 0.1, 0.1, 1.0 };
    GLfloat diffuse[]   = { 0.1, 0.6, 0.3, 1.0 };
    GLfloat specular[]  = { 1.0, 1.0, 1.0, 1.0 };
    GLfloat emission[]  = { 0.0, 0.0, 0.0, 1.0 };
    GLfloat shininess[] = { 32.0 };

    glMaterialfv( GL_FRONT, GL_AMBIENT,   ambient );
    glMaterialfv( GL_FRONT, GL_DIFFUSE,   diffuse );
    glMaterialfv( GL_FRONT, GL_SPECULAR,  specular );
    glMaterialfv( GL_FRONT, GL_EMISSION,  emission );
    glMaterialfv( GL_FRONT, GL_SHININESS, shininess );

    GLint Cuadros = 18;

    //para activar el piso y se desactiva al final
    glEnable(GL_TEXTURE_2D);
    glBindTexture(GL_TEXTURE_2D,texturas[1].ID);

    glPushMatrix();
    glTranslatef(-Brick, 0, -Brick);
    glBegin(GL_QUADS);
    for(int i=0;i<Cuadros;i++){
       for(int j=0;j<Cuadros;j++){
            if((i+j)%2==0){
                glColor4ub(25, 25, 112,Transparencia);
            }else{
                glColor4ub(112, 112, 112,Transparencia);
            }
                glTexCoord2f(0, 0); glVertex3f(-Lado+(i*Brick), 0, -Lado+(j*Brick));
                glTexCoord2f(1, 0); glVertex3f(-(Lado-Brick)+(i*Brick), 0, -Lado+(j*Brick));
                glTexCoord2f(1, 1); glVertex3f(-(Lado-Brick)+(i*Brick), 0, -(Lado-Brick)+(j*Brick));
                glTexCoord2f(0, 1); glVertex3f(-Lado+(i*Brick), 0, -(Lado-Brick)+(j*Brick));
        }
    }
    glEnd();
    glPopMatrix();

    glDisable(GL_TEXTURE_2D);
}

void drawLight( void ){
// Creates the display list used to draw the light.

    GLfloat light_ambient[]   = { 0.0, 0.0, 0.0, 1.0 };
    GLfloat light_diffuse[]   = { 1.0, 1.0, 1.0, 1.0 };
    GLfloat light_specular[]  = { 1.0, 1.0, 1.0, 1.0 };
    GLfloat light_emission[]  = { 1.0, 1.0, 1.0, 1.0 };
    GLfloat light_shininess[] = { 32.0 };

    glMaterialfv( GL_FRONT, GL_AMBIENT,   light_ambient );
    glMaterialfv( GL_FRONT, GL_DIFFUSE,   light_diffuse );
    glMaterialfv( GL_FRONT, GL_SPECULAR,  light_specular );
    glMaterialfv( GL_FRONT, GL_EMISSION,  light_emission );
    glMaterialfv( GL_FRONT, GL_SHININESS, light_shininess );

    glColor4ub(255, 255, 0, 0);
    glutSolidSphere( 0.2, 10, 10 );
}

void drawUmbrella (int color) {
if(color==0){
    glColor4ub(255, 20, 20, 0); //Roja
}
glPushMatrix();
glTranslatef(-5,-3,7);
glRotatef(50, 100, -250, 100);

GLfloat ambient[]   = { 0.20, 0.05, 0.05, 1.0 };
   GLfloat diffuse[]   = { 0.89, 0.64, 0.14, 1.0 };
   GLfloat specular[]  = { 0.00, 0.00, 0.00, 1.0 };
   GLfloat emission[]  = { 0.00, 0.00, 0.00, 1.0 };
   GLfloat shininess[] = { 128.0 };

   glMaterialfv( GL_FRONT, GL_AMBIENT,   ambient );
   glMaterialfv( GL_FRONT, GL_DIFFUSE,   diffuse );
   glMaterialfv( GL_FRONT, GL_SPECULAR,  specular );
   glMaterialfv( GL_FRONT, GL_EMISSION,  emission );
   glMaterialfv( GL_FRONT, GL_SHININESS, shininess );

   GLUquadricObj * qobj;
   qobj = gluNewQuadric();

   glPushMatrix();
   gluQuadricDrawStyle(qobj,GLU_FILL);

   //sombrilla
   glPushMatrix();
     glTranslatef(10, 4.8, -6);
     glRotatef(90, 1, 0, 0);
     glRotatef(45, 0, -1, 0);
     gluCylinder(qobj, 0, 4.5, 2.4, 10, 1);
   glPopMatrix();

   // mango
   glColor3ub(105,105,105);
   glPushMatrix();
     glTranslatef(10, 5, -6);
     glRotatef(95, 1, 0, 0);
     glRotatef(35, 0, -1, 0);
     gluCylinder(qobj, 0.06, 0.06, 7, 10, 1);
   glPopMatrix();

   // alambrado
   glColor3ub(105,105,105);
   glPushMatrix();
     glTranslatef(10, 4.5, -6);
     glRotatef(43, 1, 0, 0);
     glRotatef(2, 0, 1, 0);
     gluCylinder(qobj, 0.025, 0.025, 4.75, 10, 1);
   glPopMatrix();

   glPushMatrix();
     glTranslatef(10, 4.5, -6);
     glRotatef(19, 1, 0, 0);
     glRotatef(19, 0, -1, 0);
     gluCylinder(qobj, 0.025, 0.025, 4.75, 10, 1);
   glPopMatrix();

    glPushMatrix();
     glTranslatef(10, 4.5, -6);
     glRotatef(6, -1, 0, 0);
     glRotatef(45, 0, -1, 0);
     gluCylinder(qobj, 0.025, 0.025, 4.75, 10, 1);
   glPopMatrix();

    glPushMatrix();
     glTranslatef(10, 4.5, -6);
     glRotatef(45, -1, 0, 0);
     glRotatef(67, 0, -1, 0);
     gluCylinder(qobj, 0.025, 0.025, 4.75, 10, 1);
   glPopMatrix();

    glPushMatrix();
     glTranslatef(10, 4.5, -6);
     glRotatef(135, -1, 0, 0);
     glRotatef(66, 0, -1, 0);
     gluCylinder(qobj, 0.025, 0.025, 4.75, 10, 1);
   glPopMatrix();

    glPushMatrix();
     glTranslatef(10, 4.5, -6);
     glRotatef(174, -1, 0, 0);
     glRotatef(44, 0, -1, 0);
     gluCylinder(qobj, 0.025, 0.025, 4.75, 10, 1);
   glPopMatrix();

    glPushMatrix();
     glTranslatef(10, 4.5, -6);
     glRotatef(198, -1, 0, 0);
     glRotatef(20, 0, -1, 0);
     gluCylinder(qobj, 0.025, 0.025, 4.75, 10, 1);
   glPopMatrix();

    glPushMatrix();
     glTranslatef(10, 4.5, -6);
     glRotatef(223, -1, 0, 0);
     glRotatef(2, 0, 1, 0);
     gluCylinder(qobj, 0.025, 0.025, 4.75, 10, 1);
   glPopMatrix();

   glPushMatrix();
     glTranslatef(10, 4.5, -6);
     glRotatef(255, -1, 0, 0);
     glRotatef(15, 0, 1, 0);
     gluCylinder(qobj, 0.025, 0.025, 4.75, 10, 1);
   glPopMatrix();

   glPushMatrix();
     glTranslatef(10, 4.5, -6);
     glRotatef(287, -1, 0, 0);
     glRotatef(15, 0, 1, 0);
     gluCylinder(qobj, 0.025, 0.025, 4.75, 10, 1);
   glPopMatrix();

   // alambrado mas interno
   glPushMatrix();

   glColor3ub(105,105,105);
   glTranslatef(9.75,-1.425,0);
   glRotatef(45, 0, 0, 1);

   glPushMatrix();
     glTranslatef(3, 4, -6);
     glRotatef(20, 0, 1, 0);
     gluCylinder(qobj, 0.03, 0.03, 1.5, 10, 1);
   glPopMatrix();

   glPushMatrix();
     glTranslatef(3, 4, -6);
     glRotatef(35, 1, 0, 0);
     glRotatef(20, 0, 1, 0);
     gluCylinder(qobj, 0.03, 0.03, 1.5, 10, 1);
   glPopMatrix();

   glPushMatrix();
     glTranslatef(3, 4, -6);
     glRotatef(70, 1, 0, 0);
     glRotatef(20, 0, 1, 0);
     gluCylinder(qobj, 0.03, 0.03, 1.5, 10, 1);
   glPopMatrix();

   glPushMatrix();
     glTranslatef(3, 4, -6);
     glRotatef(105, 1, 0, 0);
     glRotatef(20, 0, 1, 0);
     gluCylinder(qobj, 0.03, 0.03, 1.5, 10, 1);
   glPopMatrix();

   glPushMatrix();
     glTranslatef(3, 4, -6);
     glRotatef(140, 1, 0, 0);
     glRotatef(20, 0, 1, 0);
     gluCylinder(qobj, 0.03, 0.03, 1.5, 10, 1);
   glPopMatrix();

   glPushMatrix();
     glTranslatef(3, 4, -6);
     glRotatef(175, 1, 0, 0);
     glRotatef(20, 0, 1, 0);
     gluCylinder(qobj, 0.03, 0.03, 1.5, 10, 1);
   glPopMatrix();

   glPushMatrix();
     glTranslatef(3, 4, -6);
     glRotatef(210, 1, 0, 0);
     glRotatef(20, 0, 1, 0);
     gluCylinder(qobj, 0.03, 0.03, 1.5, 10, 1);
   glPopMatrix();

   glPushMatrix();
     glTranslatef(3, 4, -6);
     glRotatef(245, 1, 0, 0);
     glRotatef(20, 0, 1, 0);
     gluCylinder(qobj, 0.03, 0.03, 1.5, 10, 1);
   glPopMatrix();

   glPushMatrix();
     glTranslatef(3, 4, -6);
     glRotatef(280, 1, 0, 0);
     glRotatef(20, 0, 1, 0);
     gluCylinder(qobj, 0.03, 0.03, 1.5, 10, 1);
   glPopMatrix();

   glPushMatrix();
     glTranslatef(3, 4, -6);
     glRotatef(315, 1, 0, 0);
     glRotatef(20, 0, 1, 0);
     gluCylinder(qobj, 0.03, 0.03, 1.5, 10, 1);
   glPopMatrix();

   glPopMatrix();

   glPopMatrix();

   glPopMatrix();

   gluDeleteQuadric(qobj);
}

void flotador(int color){
if(color==0){
    glColor3d(1,0,0);
}

    GLfloat ambient[]   = { 0.20, 0.05, 0.05, 1.0 };
   GLfloat diffuse[]   = { 0.89, 0.64, 0.14, 1.0 };
   GLfloat specular[]  = { 0.00, 0.00, 0.00, 1.0 };
   GLfloat emission[]  = { 0.00, 0.00, 0.00, 1.0 };
   GLfloat shininess[] = { 128.0 };

   glMaterialfv( GL_FRONT, GL_AMBIENT,   ambient );
   glMaterialfv( GL_FRONT, GL_DIFFUSE,   diffuse );
   glMaterialfv( GL_FRONT, GL_SPECULAR,  specular );
   glMaterialfv( GL_FRONT, GL_EMISSION,  emission );
   glMaterialfv( GL_FRONT, GL_SHININESS, shininess );


}

void drawTop(GLdouble x, GLdouble y, GLdouble z, int color) {
if(color==0){
    glColor4ub(255,255,0,0);
}
    GLUquadricObj *Cilindro = gluNewQuadric();
    gluQuadricDrawStyle(Cilindro, GLU_FILL);

    glPushMatrix();

    glTranslatef(x,y,z);
    glScalef(0.015, 0.015, 0.015);

    glRotated(spinT, 0, 1, 0);
    glRotated(inclinacion, 1, 0, 0);

    glPushMatrix();
    glColor4ub(255,255,0,0);
    glTranslatef(0, 3, 0);
    glutSolidSphere(3, 30, 30);
    glPopMatrix();

    glPushMatrix();
    glColor4ub(255,255,0,0);
    glTranslatef(0, 10, 0); //x, y, z
    glRotatef(90, 1, 0, 0);
    gluCylinder(Cilindro, 2, 2, 5, 30, 10);
    glPopMatrix();

    glPushMatrix();
    glColor4ub(254,00,00,00);
    glTranslatef(0, 10, 0); //x, y, z
    glRotatef(90, -1, 0, 0);
    gluCylinder(Cilindro, 2, 18, 26, 30, 10);
    glPopMatrix();

    glPushMatrix();
    glColor4ub(254,00,00,00);
    glTranslatef(0, 36, 0);
    glRotatef(90, -1, 0, 0);
    gluCylinder(Cilindro, 18, 22, 6, 30, 10);
    glPopMatrix();

    glPushMatrix();
    glColor4ub(255,255,0,0);
    glTranslatef(0, 42, 0);
    glRotatef(90, -1, 0, 0);
    gluCylinder(Cilindro, 22, 25, 6, 30, 10);
    glPopMatrix();

    glPushMatrix();
    glColor4ub(255,255,0,0);
    glTranslatef(0, 48, 0);
    glRotatef(90, -1, 0, 0);
    gluCylinder(Cilindro, 25, 27, 6, 30, 10);
    glPopMatrix();

    glPushMatrix();
    glColor4ub(254,00,00,00);
    glTranslatef(0, 54, 0);
    glRotatef(90, -1, 0, 0);
    gluCylinder(Cilindro, 27, 28, 6, 30, 10);
    glPopMatrix();

    glPushMatrix();
    glColor4ub(254,00,00,00);
    glTranslatef(0, 60, 0);
    glRotatef(90, -1, 0, 0);
    gluCylinder(Cilindro, 28, 28, 6, 30, 10);
    glPopMatrix();

    glPushMatrix();
    glColor4ub(255,255,0,0);
    glTranslatef(0, 66, 0);
    glRotatef(90, -1, 0, 0);
    gluCylinder(Cilindro, 28, 26, 6, 30, 10);
    glPopMatrix();

    glPushMatrix();
    glColor4ub(255,255,0,0);
    glTranslatef(0, 72, 0);
    glRotatef(90, -1, 0, 0);
    gluCylinder(Cilindro, 26, 22, 6, 30, 10);
    glPopMatrix();

    glPushMatrix();
    glColor4ub(254,00,00,00);
    glTranslatef(0, 78, 0);
    glRotatef(90, -1, 0, 0);
    gluCylinder(Cilindro, 22, 16, 6, 30, 10);
    glPopMatrix();

    glPushMatrix();
    glColor4ub(254,00,00,00);
    glTranslatef(0, 84, 0);
    glRotatef(90, -1, 0, 0);
    gluDisk(Cilindro, 0, 16, 20, 20);
    glPopMatrix();

//la parte negra
    glPushMatrix();
    glColor4ub(0,0,0,0);
    glTranslatef(0, 84.1, 0);
    glRotatef(90, -1, 0, 0);
    gluPartialDisk(Cilindro, 0, 16.1, 10, 20, 20, 50);
    glPopMatrix();

    glPushMatrix();
    glColor4ub(0,0,0,0);
    glTranslatef(0, 84.1, 0);
    glRotatef(90, -1, 0, 0);
    glRotatef(120, 0, 0, 1);
    gluPartialDisk(Cilindro, 0, 16.1, 10, 20, 20, 50);
    glPopMatrix();

    glPushMatrix();
    glColor4ub(0,0,0,0);
    glTranslatef(0, 84.1, 0);
    glRotatef(90, -1, 0, 0);
    glRotatef(240, 0, 0, 1);
    gluPartialDisk(Cilindro, 0, 16.1, 10, 20, 20, 50);
    glPopMatrix();
//
    glPushMatrix();
    glColor4ub(139,0,0,0);
    glTranslatef(0, 84.1, 0);
    glRotatef(90, -1, 0, 0);
    gluCylinder(Cilindro, 4, 4, 8, 30, 10);
    glPopMatrix();

    glPushMatrix();
    glColor4ub(139,0,0,0);
    glTranslatef(0, 92.1, 0);
    glRotatef(90, -1, 0, 0);
    gluDisk(Cilindro, 0, 4.1, 20, 20);
    glPopMatrix();

    glPopMatrix();

    gluDeleteQuadric(Cilindro);
}

void moverTrompo() {
     spinT += 3;
     if (x < -0.00078 && x > -0.0008) {
         inclinacion *= 0.995;
     } else {
         inclinacion = 25;

         Angulo += 0.005;
         if (Angulo > 360) {
             Angulo -= 360;
         }

         r = 10 - pow(2.7182, 0.1 * Angulo);
         if (0 < r) {
             x = r * cos (Angulo);
             z = r * sin (Angulo);
         }
     }
}

void drawCup(int color) {
    if(color==0){
        glColor4ub(112, 112, 112,200); //color gris
}
    Cilindro = gluNewQuadric();
    gluQuadricDrawStyle(Cilindro, GLU_FILL);

    glEnable(GL_BLEND); //para transparencia


    glPushMatrix();

    glTranslatef(7,5.6,0);
    glScalef(0.01, 0.01, 0.01);

    GLfloat x = 0;
    GLfloat y = 80.0;
    GLfloat z = 0;

    GLdouble h = 4.85; // height
    GLdouble base = 50.0;
    GLdouble top = base - h;

    for (int i = 1; i <= 8; i++) {
        glPushMatrix();
        glTranslatef(x, y, z); //x, y, z
        glRotatef(90, 1, 0, 0);
        gluCylinder(Cilindro, base, top, h, 40, 1);
        glPopMatrix();
        base -= h; top -= h; y -= h;
    }

    glPushMatrix();
    glTranslatef(x, y, z); //x, y, z
    glRotatef(90, 1, 0, 0);
    gluCylinder(Cilindro, base, base - 4, h, 40, 1);
    glPopMatrix();
    base -= 4; top -= h; y -= h;

    glPushMatrix();
    glTranslatef(x, y, z); //x, y, z
    glRotatef(90, 1, 0, 0);
    glutSolidCone(7,0, lines, 1);
    glPopMatrix();

    glPushMatrix();
    glTranslatef(x, y, z); //x, y, z
    glRotatef(90, 1, 0, 0);
    gluCylinder(Cilindro, base, base - 2, h, 40, 1);
    glPopMatrix();
    base -= 2; top -= h; y -= h;

    glPushMatrix();
    glTranslatef(x, y, z); //x, y, z
    glRotatef(90, 1, 0, 0);
    gluCylinder(Cilindro, base, base - 1, h, 40, 1);
    glPopMatrix();
    y -= h;

    base += 0.1;
    for (int i = 1; i <= 10; i++) {
        glPushMatrix();
        glTranslatef(x, y, z); //x, y, z
        glRotatef(90, 1, 0, 0);
        gluCylinder(Cilindro, base - 1, base -1, h, 40, 1);
        glPopMatrix();
        y -= h;
    }

    base -= 1; h = 3;

    glPushMatrix();
    glTranslatef(x, y, z); //x, y, z
    glRotatef(90, 1, 0, 0);
    gluCylinder(Cilindro, base, base + 0.5, h, 40, 1);
    glPopMatrix();
    base += 0.5; y -= h;

    glPushMatrix();
    glTranslatef(x, y, z); //x, y, z
    glRotatef(90, 1, 0, 0);
    gluCylinder(Cilindro, base, base + 1, h, 40, 1);
    glPopMatrix();
    base += 1; y -= h;

    glPushMatrix();
    glTranslatef(x, y, z); //x, y, z
    glRotatef(90, 1, 0, 0);
    gluCylinder(Cilindro, base, base + 2, h, 40, 1);
    glPopMatrix();
    base += 2; y -= h;

    glPushMatrix();
    glTranslatef(x, y, z); //x, y, z
    glRotatef(90, 1, 0, 0);
    gluCylinder(Cilindro, base, base + 5, h, 40, 1);
    glPopMatrix();
    base += 5; y -= h;

    glPushMatrix();
    glTranslatef(x, y, z); //x, y, z
    glRotatef(90, 1, 0, 0);
    gluCylinder(Cilindro, base, base + 7, h, 40, 1);
    glPopMatrix();
    base += 7; y -= h;

    glPushMatrix();
    glTranslatef(x, y, z); //x, y, z
    glRotatef(90, 1, 0, 0);
    gluCylinder(Cilindro, base, base + 10, h, 40, 1);
    glPopMatrix();
    base += 10; y -= h;

    //tapa
    glPushMatrix();
    glTranslatef(x, y, z); //x, y, z
    glRotatef(90, -1, 0, 0);
    glutSolidCone(30, 6, 40, 1);
    glPopMatrix();
    base += 10; y -= h;

    glPopMatrix();

    glDisable(GL_BLEND);//fin de la transparencia

    gluDeleteQuadric(Cilindro);
}

void drawBottle(int color) {
if(color==0){
    glColor4ub(112, 112, 112,200); //color gris
}
    Cilindro = gluNewQuadric();
    gluQuadricDrawStyle(Cilindro, GLU_FILL);

    glEnable(GL_BLEND); //para transparencia

    glPushMatrix();

    glTranslatef(8,5.5,0.5);
    glScalef(0.01, 0.01, 0.01);

    glPushMatrix();
    glTranslatef(0, 175, 0); //x, y, z
    glRotatef(90, 1, 0, 0);
    gluCylinder(Cilindro, 10, 15, 70, 40, 1);
    glPopMatrix();

    glPushMatrix();
    glTranslatef(0, 105, 0); //x, y, z
    glRotatef(90, 1, 0, 0);
    gluCylinder(Cilindro, 15, 35, 40, 40, 1);
    glPopMatrix();

    glPushMatrix();
    glTranslatef(0, 65, 0); //x, y, z
    glRotatef(90, 1, 0, 0);
    gluCylinder(Cilindro, 35, 35, 120, 40, 1);
    glPopMatrix();

    glPopMatrix();

    glDisable(GL_BLEND);//fin de la transparencia

    gluDeleteQuadric(Cilindro);

}

void drawTeaPot(int l){
    if(l==0){
    glColor4ub(208, 211, 212,0); //verde
    }
    glPushMatrix();
        glTranslatef(-7,0.8,8);
        glutSolidTeapot(1);
    glPopMatrix();
}

void drawBench(int l){
    if(l==0){
         glColor4ub(204,102,0,0);
    }

    GLfloat ambient[]   = { 0.20, 0.05, 0.05, 1.0 };
   GLfloat diffuse[]   = { 0.89, 0.64, 0.14, 1.0 };
   GLfloat specular[]  = { 0.00, 0.00, 0.00, 1.0 };
   GLfloat emission[]  = { 0.00, 0.00, 0.00, 1.0 };
   GLfloat shininess[] = { 128.0 };

   glMaterialfv( GL_FRONT, GL_AMBIENT,   ambient );
   glMaterialfv( GL_FRONT, GL_DIFFUSE,   diffuse );
   glMaterialfv( GL_FRONT, GL_SPECULAR,  specular );
   glMaterialfv( GL_FRONT, GL_EMISSION,  emission );
   glMaterialfv( GL_FRONT, GL_SHININESS, shininess );

    Cilindro = gluNewQuadric();
    gluQuadricDrawStyle(Cilindro, GLU_FILL);

    glPushMatrix();
        glScaled(0.05,0.05,0.05);
        glTranslatef(150,100,0);

    //asiento
    glPushMatrix();
        glScaled(1,0.1,1);
        glutSolidSphere(30,50,10);
    glPopMatrix();

    //patas |
    glPushMatrix();
        glTranslatef(-15,0,15);
        glRotatef(85, 1, 0, 0);
        gluCylinder(Cilindro, 2, 2, 95, 10, 1);
    glPopMatrix();

    glPushMatrix();
        glTranslatef(15,0,-15); //fondo, abajo, derixq
        glRotatef(95, 1, 0, 0);
        gluCylinder(Cilindro, 2, 2, 95, 10, 1);
    glPopMatrix();

    glPushMatrix();
        glTranslatef(-15,0,-15);
        glRotatef(95, 1, 0, 0); //cambiar menos 1 a uno
        gluCylinder(Cilindro, 2, 2, 95, 10, 1);
    glPopMatrix();

    glPushMatrix();
        glTranslatef(15,0,15); //fondo, abajo, derixq
        glRotatef(85, 1, 0, 0);
        gluCylinder(Cilindro, 2, 2, 95, 10, 1);
    glPopMatrix();

    //sosten de las patas -
    glPushMatrix();
        glTranslatef(-14,-60,20);//*
        glRotatef(90, 0, 1, 0);
        gluCylinder(Cilindro, 2, 2, 30, 10, 1);
    glPopMatrix();

    glPushMatrix();
        glTranslatef(-15,-60,20); //fondo, abajo, derixq
        glRotatef(180, 0, 1, 0);
        gluCylinder(Cilindro, 2, 2, 40, 10, 1);
    glPopMatrix();

    glPushMatrix();
        glTranslatef(-15,-60,-20); //*
        glRotatef(90, 0, 1, 0);
        gluCylinder(Cilindro, 2, 2, 30, 10, 1);
    glPopMatrix();

    glPushMatrix();
        glTranslatef(15,-60,20); //fondo, abajo, derixq
        glRotatef(180, 0, 1, 0);
        gluCylinder(Cilindro, 2, 2, 40, 10, 1);
    glPopMatrix();

    glPopMatrix();

    gluDeleteQuadric(Cilindro);
}

void Spotlight() {

    GLfloat light_Ka[]={0, 0, 0, 1}; //Color de la luz 1, 1, 1 = Blanco 1, 1, 0 = amarillo
    GLfloat light_Kd[]={4, 4, 4, 4};
    GLfloat light_Ks[]={1, 1, 1, 1};
    GLfloat light_model_Ka[]={.2, .2, .2, 1};
    GLfloat pos_light[4]={xh, yh, zh, 1};
    GLfloat dir_light[4]={0,-1,0,1};

    GLfloat material_Ka[]={.5, .5, .5, 1};
    GLfloat material_Kd[]={.4, .6, .8, 1};
    GLfloat material_Ks[]={.5, .5, .5, 1};
    GLfloat material_Ke[]={0, 0, 0, 1};
    GLfloat material_Se=10;

    glEnable(GL_DEPTH_TEST);
    glDepthFunc(GL_LEQUAL);
    glHint(GL_PERSPECTIVE_CORRECTION_HINT, GL_NICEST);

    glEnable(GL_LIGHTING);
    glEnable(GL_NORMALIZE);

    glEnable(GL_LIGHT1);
    glLightfv(GL_LIGHT1,GL_AMBIENT,light_Ka);
    glLightfv(GL_LIGHT1,GL_DIFFUSE,light_Kd);
    glLightfv(GL_LIGHT1,GL_SPECULAR,light_Ks);
    glLightf(GL_LIGHT1,GL_SPOT_CUTOFF,20);
    glLightf(GL_LIGHT1,GL_SPOT_EXPONENT,0); //valores [0, 128]
    glLightfv(GL_LIGHT1,GL_POSITION,pos_light);
    glLightfv(GL_LIGHT1,GL_SPOT_DIRECTION,dir_light);

   // glLightModelfv(GL_LIGHT_MODEL_AMBIENT,light_model_Ka);
    glLightModelf(GL_LIGHT_MODEL_LOCAL_VIEWER,1);
    glLightModelf(GL_LIGHT_MODEL_TWO_SIDE,1);

    glMaterialfv(GL_FRONT,GL_AMBIENT,material_Ka);
    glMaterialfv(GL_FRONT,GL_DIFFUSE,material_Kd);
    glMaterialfv(GL_FRONT,GL_SPECULAR,material_Ks);
    glMaterialfv(GL_FRONT,GL_EMISSION,material_Ke);
    glMaterialf(GL_FRONT,GL_SHININESS,material_Se);
}

void ActivaPlanVuelo(void){
    mDesplazamiento += IncVelocidad;
    Spotlight();
    Transparencia=255;
    switch(Direccion[k]){
    case 'A':
        AnguloX += IncVelocidad;
        break;
    case 'a':
        AnguloX -= IncVelocidad;
        break;
    case 'B':
        AnguloY += IncVelocidad;
        break;
    case 'b':
        AnguloY -= IncVelocidad;
        break;
    case 'C':
        AnguloZ += IncVelocidad;
        break;
    case 'c':
        AnguloZ -= IncVelocidad;
        break;
    case 'X':
        xh += IncVelocidad;
        break;
    case 'x':
        xh -= IncVelocidad;
        break;
    case 'Y':
        yh += IncVelocidad;
        break;
    case 'y':
        yh -= IncVelocidad;
        break;
    case 'Z':
        zh += IncVelocidad;
        break;
    case 'z':
        zh -= IncVelocidad;
        break;
    case 'L':
        if(Desplazamiento[k]==1)
            laserControl = 1;
        else
            laserControl = 0;
        break;
    case 'M':
        if(Desplazamiento[k]==1)
            misilControl = 1;
        else
            misilControl = 0;
        break;
    case 'E':
        if(Desplazamiento[k]==1)
            heliceControl = 1;
        else
            heliceControl = 0;
        break;
    }
    glRotatef(AnguloX,1,0,0);
    glRotatef(AnguloY,0,1,0);
    glRotatef(AnguloZ,0,0,1);

    glTranslatef(xh,yh,zh);

    if(Desplazamiento[k]< mDesplazamiento){
        mDesplazamiento = 0;
        ++k;
    }
    if(k>MaxPunt)
        PlanVuelo=0;
}

void drawHelicopter2(int l) {
   GLUquadricObj *Cilindro = gluNewQuadric();
if(PlanVuelo==1){
        ActivaPlanVuelo();
        }
        else{
        glRotatef(AnguloX,1,0,0);
        glRotatef(AnguloY,0,1,0);
        glRotatef(AnguloZ,0,0,1);
        glTranslatef(xh,yh,zh);
        }
   glPushMatrix();
     glColor4ub(0, 0, 255, 0);
     gluQuadricDrawStyle(Cilindro, GLU_FILL);
     glTranslatef(-25, 0, 0);
     glRotatef(90, 0, -1, 0);
     gluCylinder(Cilindro, 6, 3, 40, 10, 1);
   glPopMatrix();

   glPushMatrix();
     glColor4ub(0, 0, 255, 0);
     glTranslatef(-65, 0, 0);
     glRotatef(90, 0, -1, 0);
     gluDisk(Cilindro, 0, 3, 30, 30);
   glPopMatrix();

   gluDeleteQuadric(Cilindro);

   glPushMatrix();
     glColor4ub(0, 0, 255, 0);
     glRotatef(90, 0, 1, 0);
     glScalef(0.7, 0.7, 1);
     glutWireSphere(25, 30, 20);
   glPopMatrix();

     //cono
     glColor4ub(255, 0, 0, 0);
     glPushMatrix();
         glTranslatef(0,20.5,0);
         glRotatef(90,-1,0,0);
         glutSolidCone(2,0.5,10,1);
     glPopMatrix();

     glPushMatrix();
         glTranslatef(0,20,0);
         glRotatef(90,1,0,0);
         gluCylinder(Cilindro, 1, 1, 2.5, 20, 1);
     glPopMatrix();

     //Helice
     glColor4ub(204,204,0, 0);

     glPushMatrix();
     glTranslatef(0, 20, 0);
     glRotatef(90, 1, 0, 0);

     glPushMatrix();
     glRotatef(spinH, 0, 0, 1); //Cada vez que pase por aqu� va a rotar

     glPushMatrix();
       glScalef(5, 50, 1);
       glutSolidCube (1);
     glPopMatrix();

     glPushMatrix();
       glScalef(50, 5, 1);
       glutSolidCube (1);
     glPopMatrix();
     glPopMatrix();
     glPopMatrix();

     //H�lice trasera
     glPushMatrix();
     glTranslatef(-60, 0, -7);

     glColor4ub(255, 0, 0, 0);

     glPushMatrix();
         glTranslatef(0,0,0);
         glRotatef(90,0,0,1);
         gluCylinder(Cilindro, 1, 1, 4, 20, 1);
     glPopMatrix();

     glPushMatrix();
         glTranslatef(0,0,-0.6);
         glRotatef(180,1,0,0);
         glutSolidCone(2,0.5,10,1);
     glPopMatrix();

     glColor4ub(204,204,0, 0);   //Amarrillo
     glPushMatrix();
     glRotatef(spinH, 0, 0, 1); //Cada vez que pase por aqu� va a rotar

     glPushMatrix();
       glScalef(3, 30, 1);
       glutSolidCube (1);
     glPopMatrix();

     glPushMatrix();
       glScalef(30, 3, 1);
       glutSolidCube (1);
     glPopMatrix();

     glPopMatrix();
     glPopMatrix();

     // coleta
     glPushMatrix();
     glColor4ub(0, 0, 255, 0);

    glPushMatrix();
         glTranslatef(-67,14, 0);
         glRotatef(90,1,0,0);
         glRotatef(25,0,1,0);
         glScalef(1,0.5,1);
         gluCylinder(Cilindro, 0.8, 3, 15, 20, 1);
     glPopMatrix();

     glPushMatrix();
         glTranslatef(-66,-10, 0);
         glRotatef(90,-1,0,0);
         glRotatef(25,0,1,0);
         glScalef(1,0.5,1);
         gluCylinder(Cilindro, 0.8, 3, 15, 20, 1);
     glPopMatrix();

     glPushMatrix();
         glTranslatef(-62.5,-0.4, 13);
         glRotatef(180,-1,0,0);
         glRotatef(3,0,1,0);
         glScalef(1,0.5,1);
         gluCylinder(Cilindro, 0.8, 3, 15, 20, 1);
     glPopMatrix();

   glPopMatrix();

    glColor4ub(180, 180, 180, 180);

    // barras horizontales
    glPushMatrix();
       glTranslatef(-20,-21,12.3);
       glRotatef(90,0,1,0);
       gluCylinder(Cilindro, 1.2, 1.2, 40, 20, 1);
    glPopMatrix();

    glPushMatrix();
       glTranslatef(-20,-21,-12.3);
       glRotatef(90,0,1,0);
       gluCylinder(Cilindro, 1.2, 1.2, 40, 20, 1);
    glPopMatrix();

    // barras inclinadas
    glPushMatrix();
        glRotatef(60,1,0,0);
        glTranslatef(-7,0,17);
        gluCylinder(Cilindro, 1, 1, 8, 20, 1);
    glPopMatrix();

   glPushMatrix();
       glRotatef(60,1,0,0);
       glTranslatef(7,0,17);
       gluCylinder(Cilindro, 1, 1, 8, 100, 1);
   glPopMatrix();

   glPushMatrix();
       glTranslatef(-7,-15,-8.7);
       glRotatef(120,1,0,0);
       gluCylinder(Cilindro, 1, 1, 8, 20, 1);
   glPopMatrix();

   glPushMatrix();
       glTranslatef(7,-15,-8.7);
       glRotatef(120,1,0,0);
       gluCylinder(Cilindro, 1, 1, 8, 20, 1);
   glPopMatrix();

   //esferas
   glPushMatrix();
       glColor4ub(255,0,0,0);

       glPushMatrix();
       glTranslatef(-20,-21,12.4);
       glutSolidSphere(1.5,30,20);
       glPopMatrix();

       glPushMatrix();
       glTranslatef(-20,-21,-12.4);
       glutSolidSphere(1.5,30,20);
       glPopMatrix();

       glPushMatrix();
       glTranslatef(20,-21,-12.4);
       glutSolidSphere(1.5,30,20);
       glPopMatrix();

       glPushMatrix();
       glTranslatef(20,-21,12.4);
       glutSolidSphere(1.5,30,20);
       glPopMatrix();

       // mira
        if (misilControl == 1) {
       glColor4ub(255,0,0,0);

       glPushMatrix();
       glTranslatef(30+vellaser,-21,-12.4);
       glutSolidSphere(0.85,30,20);
       glPopMatrix();

       glPushMatrix();
       glTranslatef(30+vellaser,-21,12.4);
       glutSolidSphere(0.85,30,20);
       glPopMatrix();
        }

        //Activa y desactiva el rayo l�ser
        if (laserControl == 1) {
            glBegin(GL_LINES);
                glColor4ub(255, 31, 31, 0);
                glVertex3f( 150.0f, -20.0f, -12.0f ); glVertex3f( 0.0f, -20.0f, -12.0f ); //L�nea Norte a Sur
                glVertex3f( 150.0f, -20.0f,  12.0f ); glVertex3f( 0.0f, -20.0f,  12.0f ); //L�nea Norte a Sur
            glEnd ();
        }

    glPopMatrix();

    glPopMatrix();
}

void drawHelicopter() {
   glPushMatrix();
   glTranslated(xh, yh, zh);
   glRotatef(-180,0,1,0);
   glScalef(0.08,0.08,0.08);

   drawHelicopter2(0);
   glPopMatrix();
}

void moverHelicoptero() {
    if (spinH < 2000) {
        spinH *= 1.005;
    } else {
        spinH += 10;
    }

    if (spinH > 2700 && yh < 9)
        yh += 0.005;

    if (xh > 2 && xh < 6)
        miraControl = 1;
    else
        miraControl = 0;

    if (yh >= 9) {
        xh += 0.02;
        zh -= 0.02;
    }

}

void drawPhotography(int color){
    if(color==0){
        glColor4ub(255,255,255,255);
    }
    glEnable(GL_TEXTURE_2D);
   glPushMatrix();
   glBindTexture(GL_TEXTURE_2D,texturas[0].ID);

   glTranslatef(-4,6.7,9);

   glRotatef(45, 0, 1, 0);
   glRotatef(180, 0, 1, 0);
   glRotatef(30, -1, 0, 0);
   glScalef(0.15, 0.15, 0.15);

   glPushMatrix();
       glColor4ub(255,255,255,255);
       glTranslatef(5, 5.5, -0.12);
       glScalef(13, 16, 0.2);
       glutSolidCube(1);
   glPopMatrix();

   glPushMatrix();
       glColor4ub(255,255,255,255);
       glTranslatef(5, 5.5, -3.75);
       glRotated(45, 1, 0, 0);
       glScalef(4,10,0.2);
       glutSolidCube(1);
    glPopMatrix();

    glPushMatrix();
       glColor4ub(255,255,255,255);
       glTranslatef(-0.75, 5.5, 0.25);
       glRotatef(90, 0, 0, 1);
       glScalef(16, 1.75, 0.75);
       glutSolidCube(1);
    glPopMatrix();

    glPushMatrix();
       glColor4ub(255,255,255,255);
       glTranslatef(10.75, 5.5, 0.25);
       glRotatef(90, 0, 0, 1);
       glScalef(16, 1.75, 0.75);
       glutSolidCube(1);
    glPopMatrix();

    glPushMatrix();
       glColor4ub(255,255,255,255);
       glTranslatef(10.75, 5.5, 0.25);
       glRotatef(90, 0, 0, 1);
       glScalef(16, 1.75, 0.75);
       glutSolidCube(1);
    glPopMatrix();

    glPushMatrix();
       glColor4ub(255,255,255,255);
       glTranslatef(5, 12.6, 0.25);
       glRotatef(180, 1, 0, 0);
       glScalef(11, 1.75, 0.75);
       glutSolidCube(1);
    glPopMatrix();

    glPushMatrix();
       glColor4ub(255,255,255,255);
       glTranslatef(5, -1.3, 0.25);
       glRotatef(180, 1, 0, 0);
       glScalef(11, 2.25, 0.75);
       glutSolidCube(1);
    glPopMatrix();

    glScalef(10, 12, 10);
    glColor4ub(255, 255, 255, 255);
    glBegin(GL_QUADS);
       glTexCoord2f(0, 0); glVertex2f(0, 0); //Punto 1
       glTexCoord2f(1, 0); glVertex2f(1, 0); //Punto 2
       glTexCoord2f(1, 1); glVertex2f(1, 1); //Punto 3
       glTexCoord2f(0, 1); glVertex2f(0, 1); //Punto 4
    glEnd();

    glPopMatrix();

    glDisable(GL_TEXTURE_2D);
}

void drawTable(int l){

    Cilindro = gluNewQuadric();
    gluQuadricDrawStyle(Cilindro, GLU_FILL);
if(l==0){
    glColor4ub(204,102,0,0);
}
    glPushMatrix();
        glScaled(0.05,0.05,0.05);
        glTranslatef(-90,126,200);

    //parte de arriba
    glPushMatrix();
        glScaled(1,0.1,1);
        glutSolidSphere(55,50,10);
    glPopMatrix();

    //pata
    glPushMatrix();//<
        glTranslatef(0,0,0);
        glRotatef(85, 1, 0, 0);
        gluCylinder(Cilindro, 30, 2, 15, 10, 1);
    glPopMatrix();

    glPushMatrix(); //|
        glTranslatef(0,-13,0);
        glRotatef(90, 1, 0, 0);
        gluCylinder(Cilindro, 4, 5, 33, 10, 1);
    glPopMatrix();

    glPushMatrix(); //<
        glTranslatef(0,-53,0);
        glRotatef(270, 1, 0, 0);
        gluCylinder(Cilindro, 10, 2, 10, 10, 1);
    glPopMatrix();

    glPushMatrix();//-
        glTranslatef(0,-53,0);
        glRotatef(90, 1, 0, 0);
        gluCylinder(Cilindro, 10, 10, 3, 10, 1);
    glPopMatrix();

    glPushMatrix(); //>
        glTranslatef(0,-56,0);
        glRotatef(90, 1, 0, 0);
        gluCylinder(Cilindro, 10, 2, 10, 10, 1);
    glPopMatrix();

    //esfera
    glPushMatrix();
        glTranslatef(0,-85,0);
        glutSolidSphere(20,50,10);
    glPopMatrix();

    //parte de abajo
    glPushMatrix();
        glTranslatef(0,-110,0);
        glScaled(1,0.2,1);
        glutSolidSphere(25,50,10);
    glPopMatrix();

    glPushMatrix();
        glTranslatef(0,-120,0);
        glScaled(1,0.2,1);
        glutSolidSphere(35,50,10);
    glPopMatrix();

    glPopMatrix();
}

void drawBall(int l) {
if(l==0){
  glColor4ub(255, 255, 255, 0);
  glEnable(GL_TEXTURE_2D);
}
  glPushMatrix();

  glTranslatef(-2, 7.7, -2);

  glRotatef(90, 1, 0, 0);
  glRotatef(rotate, 0, 0, 1);


  glBindTexture(GL_TEXTURE_2D,texturas[3].ID);

  GLUquadricObj *sphere=NULL;
  sphere = gluNewQuadric();
  gluQuadricDrawStyle(sphere, GLU_FILL);
  gluQuadricTexture(sphere, TRUE);
  gluQuadricNormals(sphere, GLU_SMOOTH);

  glPushMatrix();
    glTranslatef(-7+xBall,-6.3+yBall,7+zBall);
    if(BallControl==2)
        glRotatef(BallRot,0,0,-1);
    gluSphere(sphere, 0.7, 50, 50);
  glPopMatrix();

  //glColor4ub(255, 255, 255, 0);

  glPopMatrix();

  gluDeleteQuadric(sphere);

  glDisable(GL_TEXTURE_2D);
}

void drawLamp(int l) {
   GLUquadricObj *qobj; //Define el objeto
   qobj = gluNewQuadric();
   glPushMatrix();
   glTranslatef(0, -15, 0);

   glColor4ub(255, 0, 0, 0); //Rojo
   glPushMatrix(); //Dibula el cono de la l�mpara
   glTranslatef(0, 20, 0);
    glPushMatrix();
     glTranslatef(rX, rY-7.5, rZ);
     glRotatef(90, 1, 0, 0);
     gluCylinder(qobj, 0, 1, 1.25, 30, 1);
   glPopMatrix();
   glPopMatrix();

   glPushMatrix(); //Dibuja el bombillo
    glTranslatef(0, 19, 0);
    glPushMatrix();
     glColor4ub(255,255,0,0);
     glTranslatef(rX, rY-7.5, rZ);
     glutSolidSphere(0.3, 50, 50);
   glPopMatrix();
   glPopMatrix();

   glPopMatrix();

   gluDeleteQuadric(qobj);
}

void drawScene2() {
  GLfloat shadow_plane[] = { 0.0, 1.0, 0.0, 0.0 }; //plano para proyectar la sombra
  glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT | GL_STENCIL_BUFFER_BIT);

  GLfloat mat_specular[] = { 1.0, 1.0, 1.0, 1.0 };
  GLfloat diffuseMaterial[] = { 1.0, 1.0, 1.0, 0.0 };
  GLfloat lmodel_ambient[] = { Luz, Luz, Luz, Luz };

  GLfloat lightColor[] = {1.0f, 1.0f, 1.0f, 1.0f};//Amarilllo
  GLfloat lightPos[] = {rX, rY, rZ, 1};

  glLightfv(GL_LIGHT2, GL_DIFFUSE, lightColor);
  glLightfv(GL_LIGHT2, GL_POSITION, lightPos);

  glMaterialfv(GL_FRONT, GL_DIFFUSE, diffuseMaterial);
  glMaterialfv(GL_FRONT, GL_SPECULAR, mat_specular);
  glMaterialf(GL_FRONT, GL_SHININESS, 65.0f);
  glLightModelfv(GL_LIGHT_MODEL_AMBIENT, lmodel_ambient);

  GLfloat spot_direction[] = { dx, dy, dz };
   glLightf(GL_LIGHT2, GL_SPOT_CUTOFF, 45.0);
   glLightfv(GL_LIGHT2, GL_SPOT_DIRECTION, spot_direction);
   glLightf(GL_LIGHT2, GL_SPOT_EXPONENT, 2.0);

   //printf("dx=%d dy=%d dz=%d \n", dx, dy, dz);

  glMatrixMode(GL_MODELVIEW);
  glLoadIdentity();

  glTranslatef(0, 0, -40);
  glRotatef(30, 1, 0, 0);

  glScalef(Zoom, Zoom, Zoom);

  glRotatef(spinX, 1, 0, 0);
  glRotatef(spinY, 0, 1, 0);

    //glRotatef(orbitDegrees, 0, 1, 0);
    if (Rot == 1)
      glRotatef(orbitDegrees, 0, 1, 0);

      glRotatef(2, 0, 1, 0); // orbit the Y axis

  glLightfv(GL_LIGHT0, GL_DIFFUSE, lightColor);
  glLightfv(GL_LIGHT0, GL_POSITION, lightPos);

  glMaterialfv(GL_FRONT, GL_DIFFUSE, diffuseMaterial);
  glMaterialfv(GL_FRONT, GL_SPECULAR, mat_specular);
  glMaterialf(GL_FRONT, GL_SHININESS, 64.0f);
  glLightModelfv(GL_LIGHT_MODEL_AMBIENT, lmodel_ambient);

  glPushMatrix();
    glTranslatef(0, BOX_HEIGHT, 0);
    //Aqu� debe llamar a los objetos
    drawUmbrella(0);
    flotador(0);
    //drawTop(x,y,z,0);
    drawTeaPot(0);
    drawCup(0);
    drawBottle(0);
    glPushMatrix();
        glTranslated(-2,6,-2);//fondo, abajo, derixq
        glRotated(90,1,0,0);
        drawCup(1);
    glPopMatrix();
    drawBench(0);
    drawHelicopter();
    drawTable(1);
    drawPhotography(1);
    drawBall(0);
    drawLamp(0);
  glPopMatrix();

  glEnable(GL_STENCIL_TEST); //Enable using the stencil buffer
  glColorMask(0, 0, 0, 0); //Disable drawing colors to the screen
  glDisable(GL_DEPTH_TEST); //Disable depth testing
  glStencilFunc(GL_ALWAYS, 1, 1); //Make the stencil test always pass

  //Make pixels in the stencil buffer be set to 1 when the stencil test passes
  glStencilOp(GL_KEEP, GL_KEEP, GL_REPLACE);

  //Aqu� se dibuja el piso
  drawFloor();

  glColorMask(1, 1, 1, 1); //Enable drawing colors to the screen
  glEnable(GL_DEPTH_TEST); //Enable depth testing

  //Make the stencil test pass only when the pixel is 1 in the stencil buffer
  glStencilFunc(GL_EQUAL, 1, 1);
  glStencilOp(GL_KEEP, GL_KEEP, GL_KEEP); //Make the stencil buffer not change

  //Draw the cube, reflected vertically, at all pixels where the stencil
  //buffer is 1
  glPushMatrix();
    glScalef(1, -1, 1);
    glTranslatef(0, BOX_HEIGHT, 0);
    //Aqu� se dibujan los objetos que se van a reflejar
    drawUmbrella(1);
    flotador(1);
    //drawTop(x,y,z,1);
    drawTeaPot(0);
    drawCup(1);
    drawBottle(1);
    glPushMatrix();
        glTranslated(-2,-6.5,-2);
        glRotated(90,1,0,0);
        drawCup(1);
    glPopMatrix();
    drawBench(1);
    drawHelicopter();
    drawTable(1);
    drawPhotography(1);
    drawBall(1);
    drawLamp(1);

  glPopMatrix();

  glDisable(GL_STENCIL_TEST); //Disable using the stencil buffer
  //Blend the floor onto the screen
  glEnable(GL_BLEND);

  //Aqu� se dibuja el piso que refleja
  drawFloor();
  glDisable(GL_BLEND);

  glutSwapBuffers();
}

void drawScene() {
  //Plano para proyectar la sombra
  GLfloat shadow_plane[] = { 0.0, 1.0, 0.0, 0.0 };

  glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT | GL_STENCIL_BUFFER_BIT);

  glMatrixMode(GL_MODELVIEW);
  glLoadIdentity();

  glTranslatef(0, 0, -40);
  glRotatef(30, 1, 0, 0);

  glScalef(Zoom, Zoom, Zoom);

  glRotatef(spinX, 1, 0, 0);
  glRotatef(spinY, 0, 1, 0);

  glLightfv( GL_LIGHT0, GL_POSITION, light_position );
  glPushMatrix();
    glTranslatef( light_position[0], light_position[1], light_position[2] );
    drawHelicopter();
  glPopMatrix();

    if ( shadowOn && stencilOn ){
       glEnable( GL_STENCIL_TEST );
       glStencilFunc( GL_ALWAYS, 0x1, 0xffffffff );
       glStencilOp( GL_REPLACE, GL_REPLACE, GL_REPLACE );
    }

    drawFloor();

    // Draw shadow of object.  To do this we will only draw
    // where the stencil buffer values are equal to 1.  Furthermore,
    // to avoid "dark shadows" where more than one polygon projects to
    // the same place, we only draw each pixel of the shadow once; this
    // is done by setting the stencil value to 0 when we draw.

    if ( shadowOn ){
       glDisable( GL_LIGHTING );
       glDisable( GL_DEPTH_TEST );

        if ( stencilOn ){
            glEnable( GL_STENCIL_TEST );
            glStencilFunc( GL_EQUAL, 0x1, 0xffffffff );
            glStencilOp( GL_KEEP, GL_KEEP, GL_ZERO );
        }

        // Set up for blending and define the "shadow color".  We use black,
        // but set the alpha value so that this will be blended with the
        // color already in the color buffer.

        glEnable( GL_BLEND );
        glBlendFunc( GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA );
        glColor4f( 0.0, 0.0, 0.0, shadowAlpha ); //color de la sombras

        // Draw the actual shadow by modifying the MODELVIEW matrix and
        // then drawing the object that casts the shadow.

        glPushMatrix();
          shadowTransform( shadow_plane, light_position );
          drawCup(1);
          //drawLamp(1);
          drawTable(1);
          //drawPhotography(1);
          drawBall(1);
          drawTeaPot(1);
          drawBench(1);
          flotador(1);
          drawUmbrella(1);

    //drawTop(x,y,z,1);

    drawBottle(1);
//    glPushMatrix();
//        glTranslated(-2,-6,-2);//fondo, abajo, derixq
//        glRotated(90,1,0,0);
//        drawCup(1);
//    glPopMatrix();

    //drawHelicopter();

        glPopMatrix();

        glDisable( GL_BLEND );
        glDisable( GL_STENCIL_TEST );
        glEnable( GL_DEPTH_TEST );
        glEnable( GL_LIGHTING );
    }

    // Draw the object.  With the exception of the call to
    // shadowTransform() the transformations here should match those
    // given above when the shadow was drawn.

    drawUmbrella(0);
    flotador(0);
    //drawTop(x,y,z,1);
    drawTeaPot(0);
    drawCup(0);
    drawBottle(0);
//    glPushMatrix();
//        glTranslated(-3,0.5,-1);//fondo, abajo, derixq
//        glRotated(90,1,0,0);
//        drawCup(0);
//    glPopMatrix();
    drawBench(0);
    //drawHelicopter();
    drawTable(0);
    drawPhotography(0);
    drawBall(0);
    drawLamp(0);

    // done drawing
    glutSwapBuffers();
}

void handleKeypress(unsigned char key, int x, int y) {
  switch (key) {
    case 27: //Escape key
         exit(0);
    break;

    case 'Q': //Helicoptero para atras
        xh += IncVelocidad;
        break;
    case 'q':  //Helicoptero para delante
        xh -= IncVelocidad;
        break;
    case 'w': //Helicoptero para arriba
        yh += IncVelocidad;
        break;
    case 'W': //Helicoptero para arriba
        yh -= IncVelocidad;
        break;
    case 'E': //Helicoptero para izq
        zh += IncVelocidad;
        break;
    case 'e': //Helicoptero para der
        zh -= IncVelocidad;
        break;

    case 'U':
        AnguloX += IncVelocidad;
        break;
    case 'u':
        AnguloX -= IncVelocidad;
        break;
    case 'I':
        AnguloY += IncVelocidad;
        break;
    case 'i':
        AnguloY -= IncVelocidad;
        break;
    case 'Y':
        AnguloZ += IncVelocidad;
        break;
    case 'y':
        AnguloZ -= IncVelocidad;
        break;

    case 'h':
        if(heliceControl==0)
            heliceControl=1;
        else
            heliceControl=0;
    break;

    case 'v':
        BallControl=1;
    break;

    case 'b': //balon en linea recta
        BallControl=2;
    break;

    case 'B': //balon que salte
        BallControl=3;
    break;

    case 'c':
        activarTrompo = true;
    break;

    case 'r': //La esfera gira
         if (Rot == 1)
             Rot = 0;
         else
             Rot = 1;
    break;

    case 'l': //Luz
         Luz -= 0.01;
         if (Luz < -2)
             Luz = 1;
    break;

    case 'L': //Luz
         Luz += 0.01;
         if (Luz > 1)
             Luz = 0;
    break;

    case 'z':
        if (Zoom > 0)
            Zoom -= 0.01;
    break;

    case 'Z':
        if (Zoom < 10)
            Zoom += 0.01;
    break;

    case 't': //Sube
         if (++Transparencia > 255)
             Transparencia = 0;
    break;

    case 'T':
        Transparencia--;
    break;

    case 's':
        if(laserControl==0)
            laserControl=1;
        else
            laserControl=0;

    case 'a':
        if(misilControl==0)
            misilControl=1;
        else
            misilControl=0;
    break;

     case 'o':
        if(PlanVuelo==0)
            PlanVuelo=1;
        else
            PlanVuelo=0;
    break;

    case 'p':
        activarHelicoptero = true;
    break;

    case '1':
    case '2':
    case '3':
    case '4':
    case '5':
    case '6':
    case '7':
    case '8':
    case '9':
      shadowAlpha = ( key - '0' ) / 10.0;
    break;

  }
}

void handleSpecialKeypress(int key, int x, int y) {

 switch (key) {
    case GLUT_KEY_LEFT:
         isLeftKeyPressed = true;
         if (!isRightKeyPressed) {
             if(rX > -25)
                --rX;
         }
    break;

    case GLUT_KEY_RIGHT:
         isRightKeyPressed = true;
         if (!isLeftKeyPressed) {
             if (rX < 25)
                ++rX;
         }
    break;

    case GLUT_KEY_UP:
         isUpKeyPressed = true;
         if (!isDownKeyPressed) {
             if (rY < 25)
                ++rY;
         }
    break;

    case GLUT_KEY_DOWN:
         isDownKeyPressed = true;
         if (!isUpKeyPressed) {
             if (rY > 0)
                --rY;
         }
    break;
    /////

 }
}

void handleSpecialKeyReleased(int key, int x, int y) {
 switch (key) {
 case GLUT_KEY_LEFT:
      isLeftKeyPressed = false;
 break;

 case GLUT_KEY_RIGHT:
      isRightKeyPressed = false;
 break;

 case GLUT_KEY_UP:
      isUpKeyPressed = false;
 break;

 case GLUT_KEY_DOWN:
      isDownKeyPressed = false;
 break;

 }
}

void init() {

  glClearColor(0, 0, 0, 0); //Fondo negro en toda la escena
  GLfloat ambient[]  = { 0.2, 0.2, 0.2, 1.0 };
    GLfloat diffuse[]  = { 1.0, 1.0, 1.0, 1.0 };
    GLfloat specular[] = { 1.0, 1.0, 1.0, 1.0 };
glEnable( GL_LIGHT0 );
    glLightfv( GL_LIGHT0, GL_AMBIENT,  ambient );
    glLightfv( GL_LIGHT0, GL_DIFFUSE,  diffuse );
    glLightfv( GL_LIGHT0, GL_SPECULAR, specular );

  glEnable(GL_DEPTH_TEST);
  glEnable(GL_LIGHTING);
  glEnable(GL_LIGHT2);
  glEnable(GL_NORMALIZE);
  glEnable(GL_COLOR_MATERIAL);
  glLightModelf( GL_LIGHT_MODEL_LOCAL_VIEWER, GL_TRUE );
  glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
 glDepthFunc( GL_LESS );
    glShadeModel ( GL_SMOOTH );

  if(!cargarTGA("C:/Users/geekMQ/Desktop/mesa trabajo/gl/sombrilla/goku.tga", &texturas[0]) ) {
     printf("Error cargando textura\n");
     exit(0); // Cargamos la textura y chequeamos por errores
	}

    if(!cargarTGA("C:/Users/geekMQ/Desktop/mesa trabajo/gl/sombrilla/Piso.tga", &texturas[1]) ) {
        printf("Error cargando textura\n");
        exit(0); // Cargamos la textura y chequeamos por errores
	}
	if(!cargarTGA("C:/Users/geekMQ/Desktop/mesa trabajo/gl/sombrilla/Star.tga", &texturas[2])) {
              printf("Error cargando textura\n");
     exit(0); // Cargamos la textura y chequeamos por errores
    }
    if(!cargarTGA("C:/Users/geekMQ/Desktop/mesa trabajo/gl/sombrilla/Bola.tga", &texturas[3])) {
              printf("Error cargando textura\n");
     exit(0); // Cargamos la textura y chequeamos por errores
    }

    //cargar plan de vuelo
    FILE *myfile;
    if((myfile=fopen("C:/Users/geekMQ/Desktop/mesa trabajo/gl/sombrilla/planVuelo.txt","r"))==NULL){
        printf("Error al abrir el plan de vuelo");
        exit(0);
    }

    k=0;
    while(fscanf(myfile,"%c %f", &Direccion[k], &Desplazamiento[k]) != EOF){
        k++;
    }

    MaxPunt = k - 1;
    k = 0;

    fclose(myfile);
}

void handleResize(int w, int h) {
  glViewport(0, 0, w, h);
  glMatrixMode(GL_PROJECTION);
  glLoadIdentity();
  gluPerspective(45.0, (float)w / (float)h, 1.0, 200.0);
}

void spinDisplay(void) {
    //
    _angle += 0.002;
   if (_angle > 360) {
       _angle -= 360;
   }

    if(BallControl==1){
        if(zBall<=0){
            yBall+=BallVelocidad;
            zBall+=BallVelocidad;
        }
        else{
            zBall-=5;
        }
    }

   if(BallControl==2){
        if(yBall<=10){
            yBall+=BallVelocidad;
        }
    }

    if(BallControl==3){
        if(zBall<=0){
            zBall+=BallVelocidad;
        }
        else{
            zBall-=5;
        }
    }

    if(misilControl==1){
        if(vellaser<=50){
            vellaser+=1;
        }
        else{
            vellaser-=50;
        }
    }

    if(heliceControl==1){
    if (spinH < 2000) {
        spinH *= 1.005;
    } else {
        spinH += 10;
    }}

    if (activarTrompo)
        moverTrompo();

//    if (activarHelicoptero) {
//        moverHelicoptero();
//    }

    orbitDegrees += 0.1;

    //de sombra
    static float theta  = 0.0;       // angle of light
    static float dtheta = 0.1 / 128.0; // angle step for light
    float dalpha = 2.0;      // angle step (in degrees) for object

    // Move the light.  Currently the light moves along an arc in the
    // xy-plane making at most angles of +pi/4+dtheta and -pi/4-dtheta
    // with the positive z-axis.  When the maximum angular deflection
    // is reached then the direction of motion is reversed.

    theta += dtheta;
    light_position[0] = 10.0 * sin( theta );
    light_position[1] = 10.0 * cos( theta );
    if ( theta > M_PI / 2.0 || theta < -M_PI / 2.0 )
        dtheta = -dtheta;

    // Update the angle used for the tumbling object.  Since this
    // angle is required by the display callback, alpha must be global.

    alpha += dalpha;
//fin de sombra
    glutPostRedisplay(); //Vuelve a dibujar
}

void mouse(int button, int state, int x, int y) {
   switch (button) {
      case GLUT_LEFT_BUTTON:
         if (state == GLUT_DOWN) {
            glutIdleFunc(spinDisplay);
         }
         break;

      case GLUT_RIGHT_BUTTON:
      case GLUT_MIDDLE_BUTTON:
         if (state == GLUT_DOWN)
            glutIdleFunc(NULL);
         break;

      default:
         break;
   }
}

void mouseMotion(int x, int y) {
     spinX = y;
     spinY = x;
     //printf("X %5.2f Y %5.2f\n", spinX, spinY);
}

int main(int argc, char** argv) {
  glutInit(&argc, argv);
  glutInitDisplayMode(GLUT_DOUBLE | GLUT_RGB | GLUT_DEPTH | GLUT_STENCIL);
  glutInitWindowSize(600, 600);

  glutCreateWindow("Escena 1");
  init();

  glutDisplayFunc(drawScene);
  glutKeyboardFunc(handleKeypress);
  glutReshapeFunc(handleResize);
  glutMouseFunc(mouse); //Activa los controles del mouse
  glutMotionFunc(mouseMotion);

  glutSpecialFunc(handleSpecialKeypress);
  glutSpecialUpFunc(handleSpecialKeyReleased);

  glutMainLoop();

  return 0;
}
